mod build_request;

use async_std::fs::File;
use async_std::io::Result as ioResult;
use async_std::os::unix::net::UnixStream;
use async_std::prelude::*;
use serde_json::Value;

use crate::build_request::Request;

#[async_std::main]
async fn main() -> ioResult<()> {
	let request = Request::new("GET", "/images/json");

	println!("after writing");

	let response = make_request(&request.as_http_string()).await?;

	let response_body: Vec<&str> = response.split("\r\n\r\n").collect();

	let json_response: Value = serde_json::from_str(response_body[1]).unwrap();

	let mut file = File::create("images.json").await?;

	file.write_all(&response_body[1].as_bytes()).await?;

	println!("Response: {:?}", json_response);

	Ok(())
}

async fn make_request(request: &String) -> ioResult<String> {
	let mut stream = UnixStream::connect("/var/run/docker.sock").await?;

	stream.write_all(request.as_bytes()).await?;

	let mut response = String::new();

	stream.read_to_string(&mut response).await?;

	Ok(response)
}
