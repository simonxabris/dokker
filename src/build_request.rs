pub struct Request {
	pub method: String,
	pub path: String,
}

impl Request {
	pub fn new(method: &str, path: &str) -> Request {
		Request {
			method: method.to_owned().to_string(),
			path: path.to_owned().to_string(),
		}
	}
	pub fn as_http_string(&self) -> String {
		format!("{} {} HTTP/1.1\r\n\r\n", self.method, self.path)
	}
}
